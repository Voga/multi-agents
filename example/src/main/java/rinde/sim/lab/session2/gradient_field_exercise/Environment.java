package rinde.sim.lab.session2.gradient_field_exercise;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import rinde.sim.core.graph.Point;
import rinde.sim.core.model.RoadModel;
import rinde.sim.lab.session2.gradient_field_exercise.trucks.*;
import rinde.sim.lab.session2.gradient_field_exercise.packages.Package;

public class Environment {

	private static Environment env;
	private ArrayList<Package> packages;
	private ArrayList<Truck> trucks;

	public synchronized static Environment getInstance() {
		if (env == null) {
			env = new Environment();
		}
		return env;
	}

	private Environment() {
		packages = new ArrayList<Package>();
		trucks = new ArrayList<Truck>();
	}

	public void registerTruck(Truck truck) {
		trucks.add(truck);
	}

	public void removeTruck(Truck truck) {
		trucks.remove(truck);
	}

	public void registerPackage(Package packageObj) {
		packages.add(packageObj);
	}

	public void removePackages(Package packageObj) {
		packages.remove(packageObj);
	}
	
	public int getAmountPackages(){
		return packages.size();
	}

	public double getRadientPower(Point point, Truck truck){
		double posPower = 0;
		double negPower = 0;
		for(Package currentPackage: packages){
			Point position = currentPackage.getPickupLocation();
			if(point != position){
				double length = calculateLength(point, position, truck.getRoadModel());
				double power = 1/length;
				posPower =+ power;
			}
			else{
				posPower =+ 1;
			}
		}
		for(Truck currentTruck: trucks){
			Point position = currentTruck.getPosition();
			if(point != position && truck != currentTruck){
				double length = calculateLength(point, position,truck.getRoadModel());
				double power = 1/length;
				negPower =+ power;
			}
			else if(truck != currentTruck){
				negPower =+ 1;
			}
		}
		return 1*posPower - 0.3*negPower;
	}

	public double calculateLength(Point begin, Point end, RoadModel model) {
		double length = 0;
		List<Point> path = model.getShortestPathTo(begin, end);
		Point point1 = path.get(0);
		path.remove(0);
		for(Point point2 : path){
			length += Math.sqrt((point2.x - point1.x) * (point2.x - point1.x) + (point2.y - point1.y)
					* (point2.y - point1.y));
			point1 = point2;
		}
		return length;		
	}
}
