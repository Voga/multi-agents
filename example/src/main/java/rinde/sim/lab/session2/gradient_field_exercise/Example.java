package rinde.sim.lab.session2.gradient_field_exercise;

import rinde.sim.lab.common.StatisticsMgr;
import rinde.sim.event.pdp.StandardType;
import rinde.sim.scenario.Scenario;
import rinde.sim.scenario.ScenarioBuilder;
import rinde.sim.scenario.TimedEvent;
import rinde.sim.util.OSM;

/**
 * 
 */
public class Example {
	
	public static void main(String[] args) throws Exception{
		ScenarioBuilder builder = new ScenarioBuilder(StandardType.ADD_TRUCK, StandardType.ADD_PACKAGE);
		
		builder.add(
				new ScenarioBuilder.MultipleEventGenerator<TimedEvent>(
						0, //at time 0
						5, //amount of trucks to be added
						new ScenarioBuilder.EventTypeFunction(
								StandardType.ADD_TRUCK
						)
				)
		);
		
		builder.add(
				new ScenarioBuilder.MultipleEventGenerator<TimedEvent>(
						0, //at time 0
						30, //amount of trucks to be added
						new ScenarioBuilder.EventTypeFunction(
								StandardType.ADD_PACKAGE
						)
				)
		);
		
//		builder.add(
//				new ScenarioBuilder.TimeSeries(
//						0, 
//						10,
//						1,
//						new ScenarioBuilder.EventTypeFunction(
//								StandardType.ADD_PACKAGE
//						)
//				)
//		);
		
		
		Scenario scenario = builder.build();
		
		final String MAP_DIR = "../core/files/maps/";
		//new SimpleController(scenario, -1, OSM.parse(MAP_DIR+"rumst.osm"));
		new SimpleController(scenario, -1, MAP_DIR + "leuven-simple.dot");		
	}
}
