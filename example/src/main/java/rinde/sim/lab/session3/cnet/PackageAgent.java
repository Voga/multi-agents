package rinde.sim.lab.session3.cnet;

import java.util.Queue;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import rinde.sim.lab.common.trucks.*;

import rinde.sim.core.Simulator;
import rinde.sim.core.SimulatorAPI;
import rinde.sim.core.SimulatorUser;
import rinde.sim.core.TickListener;
import rinde.sim.core.graph.Point;
import rinde.sim.core.model.communication.CommunicationAPI;
import rinde.sim.core.model.communication.CommunicationUser;
import rinde.sim.core.model.communication.Mailbox;
import rinde.sim.core.model.communication.Message;
import rinde.sim.lab.common.packages.Package;
import rinde.sim.lab.session3.cnet.messages.BidMessage;
import rinde.sim.lab.session3.cnet.messages.ConfirmMessage;
import rinde.sim.lab.session3.cnet.messages.RequestMessage;
import rinde.sim.lab.session3.cnet.messages.ResponseMessage;

public class PackageAgent implements TickListener, SimulatorUser, CommunicationUser {
	protected static final Logger LOGGER = LoggerFactory.getLogger(Simulator.class); 

	private SimulatorAPI simulator;
	private Package myPackage;
	private CommunicationAPI communicationAPI;
	private Mailbox mailbox;
	private double radius;
	private double reliability;
	private boolean confirmed = false;
	private boolean sended = false;
	private boolean confirmSend = false;
	
	
	public PackageAgent(Package myPackage, double radius, double reliability){
		this.myPackage = myPackage;
		this.radius = radius;
		this.reliability = reliability;
		this.mailbox = new Mailbox();
		myPackage.setAgent(this);
	}
	
	@Override
	public void setSimulator(SimulatorAPI api) {
		this.simulator = api;
	}

	@Override
	public void tick(long currentTime, long timeStep) {
		myPackage.increaseTicksAge();
		if(!sended){
			communicationAPI.broadcast(new RequestMessage(this, myPackage));
			LOGGER.info("Package agent broadcast for package "+ myPackage.getPackageID());
			sended = true;
		}
		Queue<Message> messages = mailbox.getMessages();
		for(Message message: messages){
			if(message instanceof BidMessage){
				BidMessage m = (BidMessage) message;
				TruckAgent agent = (TruckAgent) m.getSender();
				LOGGER.info("Bid added for truck "+agent.getId());
				myPackage.addbid(agent, m.getBid());
			}
			if(message instanceof ResponseMessage){
				if(((ResponseMessage) message).getAnswer() == true){
					confirmed = true;
					LOGGER.info("Package "+myPackage.getPackageID()+" confirmed");
				}
				else{
					TruckAgent sender = (TruckAgent) message.getSender();
					LOGGER.info("Package "+myPackage.getPackageID()+" failed bij truck "+sender.getId()+" reïnitilize");
					myPackage.clearBids();
					confirmSend = false;
					sended = false;
				}
			}
		}
		int truckAmount = myPackage.getRoadModel().getObjectsOfType(Truck.class).size();
		if(myPackage.getAmountBids() >= truckAmount && !confirmed && !confirmSend){
			if(myPackage.getHigh() != null){
				LOGGER.info("Agent "+myPackage.getHigh().getId()+" gets the job for package "+myPackage.getPackageID());
				ConfirmMessage newMessage = new ConfirmMessage(this, myPackage);
				communicationAPI.send(myPackage.getHigh(),newMessage);
				confirmSend = true;
			}
			else {
				LOGGER.info("Package "+myPackage.getPackageID()+" failed no truck available, reïnitilize");
				myPackage.clearBids();
				confirmSend = false;
				sended = false;
			}
		}
	}

	@Override
	public void afterTick(long currentTime, long timeStep) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setCommunicationAPI(CommunicationAPI api) {
		this.communicationAPI = api;
	}

	@Override
	public Point getPosition() {
		//TODO
		return this.myPackage.getPickupLocation();
	}

	@Override
	public double getRadius() {
		return radius;
	}

	@Override
	public double getReliability() {
		return reliability;
	}

	@Override
	public void receive(Message message) {
		this.mailbox.receive(message);
	}

}
