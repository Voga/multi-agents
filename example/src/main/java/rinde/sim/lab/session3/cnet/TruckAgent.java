package rinde.sim.lab.session3.cnet;

import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Random;

import rinde.sim.lab.common.StatisticsMgr;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import rinde.sim.core.Simulator;
import rinde.sim.core.SimulatorAPI;
import rinde.sim.core.SimulatorUser;
import rinde.sim.core.TickListener;
import rinde.sim.core.graph.Point;
import rinde.sim.core.model.RoadModel.PathProgress;
import rinde.sim.core.model.communication.CommunicationAPI;
import rinde.sim.core.model.communication.CommunicationUser;
import rinde.sim.core.model.communication.Mailbox;
import rinde.sim.core.model.communication.Message;
import rinde.sim.lab.common.SimpleMessage;
import rinde.sim.lab.common.packages.Package;
import rinde.sim.lab.common.trucks.Truck;
import rinde.sim.lab.session3.cnet.messages.BidMessage;
import rinde.sim.lab.session3.cnet.messages.ConfirmMessage;
import rinde.sim.lab.session3.cnet.messages.RequestMessage;
import rinde.sim.lab.session3.cnet.messages.ResponseMessage;

public class TruckAgent implements TickListener, SimulatorUser, CommunicationUser {

	protected static final Logger LOGGER = LoggerFactory.getLogger(Simulator.class);
	private static int globalId = 1;
	private SimulatorAPI simulator;
	private Queue<Point> path;
	private Truck truck;
	private CommunicationAPI communicationAPI;
	private double reliability, radius;
	private Mailbox mailbox;
	private Random randomGenerator = new Random();
	private PackageAgent agent;
	private int id;
	private rinde.sim.lab.common.packages.Package myPackage;
	private boolean buzy = false;
	
	public TruckAgent(Truck truck, double radius, double reliability){
		this.id = globalId;
		globalId++;
		this.truck = truck;
		this.radius = radius;
		this.reliability = reliability;
		this.mailbox = new Mailbox();
		truck.setAgent(this);
	}
	
	@Override
	public void setSimulator(SimulatorAPI api) {
		this.simulator = api;
	}
	
	public void removeTruck(){
		simulator.unregister(truck);
		simulator.unregister(this);
	}

	/**
	 * Very dumb agent, that chooses paths randomly and tries to pickup stuff and deliver stuff at the end of his paths
	 */
	@Override
	public void tick(long currentTime, long timeStep) {
		Queue<Message> messages = mailbox.getMessages();
		for(Message message: messages){
			if(message instanceof RequestMessage){
				RequestMessage m = (RequestMessage) message;
				LOGGER.info("Truck "+getId()+" received the request for package "+m.getPackagePick().getPackageID());
				if(path == null || path.isEmpty() && !buzy){
					double length = calculateLength(m.getPackagePick());
					BidMessage bidMessage = new BidMessage(this, 1/length, m.getPackagePick());
					communicationAPI.send(m.getSender(), bidMessage);
					LOGGER.info("Truck "+getId()+" answers random for package "+m.getPackagePick().getPackageID());
				}
				else{
					BidMessage bidMessage = new BidMessage(this, 0, m.getPackagePick());
					communicationAPI.send(m.getSender(), bidMessage);
					LOGGER.info("Truck "+getId()+" answers 0 for package "+m.getPackagePick().getPackageID());
				}
			}
			if(message instanceof ConfirmMessage){
				ConfirmMessage castMessage = (ConfirmMessage) message;
				if(path == null || path.isEmpty() && !buzy){
					this.path = new LinkedList<Point>(truck.getRoadModel().getShortestPathTo(truck, ((ConfirmMessage) message).getPackagePick().getPickupLocation()));
					agent = (PackageAgent) message.getSender();
					myPackage = castMessage.getPackagePick();
					buzy = true;
					LOGGER.info("Agent "+getId()+" accepts order for package "+castMessage.getPackagePick().getPackageID());
				}
				else{
					ResponseMessage responseMessage = new ResponseMessage(this, false);
					communicationAPI.send(message.getSender(), responseMessage);
					LOGGER.info("Agent "+getId()+" refuses order for package "+castMessage.getPackagePick().getPackageID());

				}
			}
			
		}		
		if((path == null || path.isEmpty()) && myPackage != null && truck.getPosition() == myPackage.getPickupLocation()){
			boolean response = truck.tryPickup();
			if(response){
				LOGGER.info("Truck "+getId()+" PickedUp package "+myPackage.getPackageID());
			}
			ResponseMessage responseMessage = new ResponseMessage(this, response);
			communicationAPI.send(agent, responseMessage);
			myPackage = null;
			agent = null;
		}
		else if(truck.hasLoad() && truck.getPosition() == truck.getLoad().getDeliveryLocation()){
			if(truck.tryDelivery()){
				buzy = false;
			}
		}
		else if(truck.hasLoad() && (path == null || path.isEmpty())){
			this.path = new LinkedList<Point>(truck.getRoadModel().getShortestPathTo(truck, truck.getLoad().getDeliveryLocation()));
			LOGGER.info("Truck "+getId()+" Delivered package "+truck.getLoad().getPackageID());
		}
		else if(path == null || path.isEmpty()){
		}
		else{
			PathProgress progress = truck.drive(path, timeStep);
			StatisticsMgr.getInstance().increaseDistanceTraveled(progress.distance);
		}
	}

	private double calculateLength(Package packagePick) {
		List<Point> path = truck.getRoadModel().getShortestPathTo(truck.getPosition(), packagePick.getPickupLocation());
		double length = 0;
		Point point1 = path.get(0);
		path.remove(0);
		for(Point point2 : path){
			length += Math.sqrt((point2.x - point1.x) * (point2.x - point1.x) + (point2.y - point1.y)
					* (point2.y - point1.y));
			point1 = point2;
		}
		return length;
	}

	@Override
	public void afterTick(long currentTime, long timeStep) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setCommunicationAPI(CommunicationAPI api) {
		this.communicationAPI = api;
	}

	@Override
	public Point getPosition() {
		return this.truck.getPosition();
	}

	@Override
	public double getRadius() {
		return this.radius;
	}

	@Override
	public double getReliability() {
		return this.reliability;
	}

	@Override
	public void receive(Message message) {
		this.mailbox.receive(message);
	}
	
	public int getId(){
		return id;
	}

}
