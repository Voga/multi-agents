package rinde.sim.lab.session3.cnet.messages;

import rinde.sim.core.graph.Point;
import rinde.sim.core.model.communication.CommunicationUser;
import rinde.sim.core.model.communication.Message;
import rinde.sim.lab.common.packages.Package;

public class RequestMessage extends Message {
	
	private Package packagePick;
	
	public RequestMessage(CommunicationUser sender, Package myPackage) {
		super(sender);
		this.packagePick = myPackage;
	}

	public Package getPackagePick() {
		return packagePick;
	}

	public void setPackagePick(Package packagePick) {
		this.packagePick = packagePick;
	}

}
