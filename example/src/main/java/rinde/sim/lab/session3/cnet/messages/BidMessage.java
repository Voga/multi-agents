package rinde.sim.lab.session3.cnet.messages;

import rinde.sim.core.model.communication.CommunicationUser;
import rinde.sim.core.model.communication.Message;
import rinde.sim.lab.common.packages.Package;

public class BidMessage extends Message {
	
	private double bid;
	private Package packagePick;
	
	public double getBid() {
		return bid;
	}

	public void setBid(double bid) {
		this.bid = bid;
	}

	public Package getPackagePick() {
		return packagePick;
	}

	public void setPackagePick(Package packagePick) {
		this.packagePick = packagePick;
	}

	public BidMessage(CommunicationUser sender, double bid, Package packagePick) {
		super(sender);
		this.bid = bid;
		this.packagePick = packagePick;
	}
	
}
