package rinde.sim.lab.session3.cnet;

import java.util.List;
import java.util.Set;

import rinde.sim.lab.common.StatisticsMgr;

import org.apache.commons.math.random.MersenneTwister;
import org.eclipse.swt.graphics.RGB;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import rinde.sim.core.Simulator;
import rinde.sim.core.graph.Graph;
import rinde.sim.core.graph.MultiAttributeEdgeData;
import rinde.sim.core.graph.Point;
import rinde.sim.core.model.Model;
import rinde.sim.core.model.RoadModel;
import rinde.sim.core.model.communication.CommunicationModel;
import rinde.sim.event.Event;
import rinde.sim.lab.common.packages.DeliveryLocation;
import rinde.sim.lab.common.packages.Package;
import rinde.sim.lab.common.trucks.Truck;
import rinde.sim.scenario.ConfigurationException;
import rinde.sim.scenario.Scenario;
import rinde.sim.scenario.ScenarioController;
import rinde.sim.serializers.DotGraphSerializer;
import rinde.sim.serializers.SelfCycleFilter;
import rinde.sim.ui.View;
import rinde.sim.ui.renderers.ObjectRenderer;
import rinde.sim.ui.renderers.UiSchema;

public class SimpleController extends ScenarioController{

	String map;
	
	private RoadModel roadModel;
	private CommunicationModel communicationModel;
	
	private int truckID = 0;
	private int packageID = 0;
	private Graph<MultiAttributeEdgeData> graph;
	protected static final Logger LOGGER = LoggerFactory.getLogger(Truck.class);
	
	public SimpleController(Scenario scen, int numberOfTicks, String map) throws ConfigurationException {
		super(scen, numberOfTicks);
		StatisticsMgr.reset();
		StatisticsMgr.getInstance().setSolutionName("Contract Net");
		this.map = map;
		
		initialize();
		StatisticsMgr.getInstance().printStatistics();
	}
	
	public SimpleController(Scenario scen, int numberOfTicks, Graph<MultiAttributeEdgeData> graph) throws ConfigurationException {
		super(scen, numberOfTicks);
		StatisticsMgr.reset();
		StatisticsMgr.getInstance().setSolutionName("Contract Net");
		this.map = map;
		this.graph = graph;
		initialize();
		StatisticsMgr.getInstance().printStatistics();
	}

	@Override
	protected Simulator createSimulator() throws Exception {
		try {
			if(graph == null)
				graph = DotGraphSerializer.getMultiAttributeGraphSerializer(new SelfCycleFilter()).read(map);
		} catch (Exception e) {
			throw new ConfigurationException("e:", e);
		}
		roadModel = new RoadModel(graph);
		MersenneTwister rand = new MersenneTwister(123);
		communicationModel = new CommunicationModel(rand, true);
		Simulator s = new Simulator(rand, 10000);
		s.register(roadModel);
		s.register(communicationModel);
		return s;	
	}
	
	@Override
	protected boolean createUserInterface() {
		UiSchema schema = new UiSchema();
		schema.add(Truck.class, "/graphics/deliverytruck.png");
		schema.add(Package.class, "/graphics/deliverypackage.png");

		View.startGui(getSimulator(), 3, new ObjectRenderer(roadModel, schema, false));

		return true;
	}

	@Override
	protected boolean handleAddTruck(Event e) {
		Truck truck = new Truck("Truck-"+truckID++, graph.getRandomNode(getSimulator().getRandomGenerator()), 500);
		getSimulator().register(truck);
		TruckAgent agent = new TruckAgent(truck, -1, 1);
		getSimulator().register(agent);
		return true;
	}	

	@Override
	protected boolean handleAddPackage(Event e){
		Point pl = graph.getRandomNode(getSimulator().getRandomGenerator());
		DeliveryLocation dl = new DeliveryLocation(graph.getRandomNode(getSimulator().getRandomGenerator()));
		getSimulator().register(pl);
		getSimulator().register(dl);
		
		Package p = new Package("Package-"+packageID++, pl, dl);
		getSimulator().register(p);
		PackageAgent agent = new PackageAgent(p, -1, 1);
		getSimulator().register(agent);
		return true;
	}
	
	@Override
	protected boolean handleRemoveTruck(Event e){
		List<Model<?>> models = getSimulator().getModels();
		RoadModel roadModel =  null;
		for(Model<?> model: models){
			if(model instanceof RoadModel){
				roadModel = (RoadModel) model;
				break;
			}
		}
		Set<Truck> trucks = null;
		if(roadModel != null){
			trucks = roadModel.getObjectsOfType(Truck.class);
			if(trucks.size() > 1){
				for(Truck truck: trucks){
					if(!truck.hasLoad()){
						truck.remove();
						LOGGER.info(truck.getTruckID()+" removed");
						return true;
					}
				}
			}
		}		
		return true;
	}

}
