package rinde.sim.lab.session3.cnet.messages;

import rinde.sim.core.model.communication.CommunicationUser;
import rinde.sim.core.model.communication.Message;

public class ResponseMessage extends Message {

	boolean succes;
	
	public ResponseMessage(CommunicationUser sender, boolean succes) {
		super(sender);
		this.succes = succes;
	}
	
	public boolean getAnswer(){
		return succes;
	}

}
