package rinde.sim.lab.session3.cnet.messages;

import rinde.sim.core.model.communication.CommunicationUser;
import rinde.sim.lab.common.packages.Package;
import rinde.sim.core.model.communication.Message;

public class ConfirmMessage extends Message {
	
	private Package packagePick;
	
	public ConfirmMessage(CommunicationUser sender, Package packagePick) {
		super(sender);
		this.packagePick = packagePick;
	}

	public Package getPackagePick() {
		return packagePick;
	}

	public void setPackagePick(Package packagePick) {
		this.packagePick = packagePick;
	}

}
