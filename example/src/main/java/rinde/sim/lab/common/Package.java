package rinde.sim.lab.common;

import java.util.HashMap;

import rinde.sim.core.graph.Point;
import rinde.sim.core.model.RoadModel;
import rinde.sim.core.model.RoadUser;
import rinde.sim.lab.session3.cnet.TruckAgent;

public class Package implements RoadUser {
	public final String name;
	private Point location;
	private HashMap<TruckAgent,Integer> bids =  new HashMap<TruckAgent, Integer>();

	public Package(String name, Point location) {
		this.name = name;
		this.location = location;
	}

	@Override
	public String toString() {
		return name;
	}

	@Override
	public void initRoadUser(RoadModel model) {
		model.addObjectAt(this, location);
	}

	public Point getLocation() {
		return location;
	}
	
	public void addbid(TruckAgent agent,Integer bid){
		bids.put(agent, bid);
	}
	
	public int getAmountBids(){
		return bids.size();
	}
	
	public TruckAgent getHigh(){
		TruckAgent maxAgent = null;
		int max = 0;
		for(TruckAgent agent:bids.keySet()){
			int bid = bids.get(agent);
			if(bid > max){
				maxAgent = agent;
				max = bid;
			}
		}
		return maxAgent;
	}
}