package rinde.sim.lab.common.packages;

import java.util.HashMap;

import rinde.sim.lab.common.StatisticsMgr;

import rinde.sim.core.SimulatorAPI;
import rinde.sim.core.SimulatorUser;
import rinde.sim.core.graph.Point;
import rinde.sim.core.model.RoadModel;
import rinde.sim.core.model.RoadUser;
import rinde.sim.lab.session3.cnet.PackageAgent;
import rinde.sim.lab.session3.cnet.TruckAgent;

public class Package implements SimulatorUser, RoadUser{
	public final String packageID;
	private Point pickupLocation;
	private DeliveryLocation deliveryLocation;
	private boolean pickedUp;
	private boolean delivered;
	private SimulatorAPI simulator;
	private RoadModel roadModel;
	private HashMap<TruckAgent,Double> bids =  new HashMap<TruckAgent, Double>();
	private int ticksAge = 0;
	private PackageAgent agent;

	public Package(String packageID, Point pickupLocation, DeliveryLocation deliveryLocation ) {
		this.packageID = packageID;
		this.pickupLocation = pickupLocation;
		this.deliveryLocation = deliveryLocation;
		this.pickedUp = false;
		this.delivered = false;
	}
	
	public int getTicksAge(){
		return ticksAge;
	}
	
	public void setAgent(PackageAgent agent){
		this.agent = agent;
	}
	
	public void increaseTicksAge(){
		ticksAge++;
	}
	
	public boolean needsPickUp(){
		return !pickedUp;
	}

	public boolean delivered(){
		return delivered;
	}
	
	public void pickup(){
		this.pickedUp = true;
		this.simulator.unregister(this);
		StatisticsMgr.getInstance().increaseTotalTicksAgePickUp(ticksAge);
	}
	
	public void deliver(){
		this.delivered = true;
		this.simulator.unregister(deliveryLocation);
		this.simulator.unregister(agent);
		StatisticsMgr.getInstance().increaseTotalTicksAgeDelivery(ticksAge);
	}
	
	public String getPackageID(){
		return packageID;
	}
	
	@Override
	public String toString() {
		return packageID;
	}

	public Point getPickupLocation(){
		return pickupLocation;
	}
	
	public Point getDeliveryLocation(){
		return deliveryLocation.getPosition();
	}

	@Override
	public void setSimulator(SimulatorAPI api) {
		this.simulator = api;
	}

	@Override
	public void initRoadUser(RoadModel model) {
		model.addObjectAt(this, pickupLocation);
		roadModel = model;
	}
	
	public RoadModel getRoadModel(){
		return roadModel;
	}
	
	public void addbid(TruckAgent agent,double bid){
		bids.put(agent, bid);
	}
	
	public int getAmountBids(){
		return bids.size();
	}
	
	public TruckAgent getHigh(){
		TruckAgent maxAgent = null;
		double max = 0;
		for(TruckAgent agent:bids.keySet()){
			double bid = bids.get(agent);
			if(bid > max){
				maxAgent = agent;
				max = bid;
			}
		}
		return maxAgent;
	}
	
	public void clearBids(){
		bids = new HashMap<TruckAgent, Double>();
	}
}
