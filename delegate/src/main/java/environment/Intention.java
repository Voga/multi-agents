package environment;

import java.util.List;

import main.Delegate;

import rinde.sim.core.graph.Point;

import agents.PackageAgent;
import agents.Truck;

public class Intention {
	private Truck truck;
	private int strength;
	private PackageAgent agent;
	private int initialIntentionStrength = 5;
	private int pickUpTimeInDistance;
	private double betterPercentage;
	
	public Intention(Truck truck, PackageAgent agent, int pickUpTimeInDistance){
		betterPercentage = Delegate.getSignificantIntentionWeight();
		this.truck = truck;
		this.strength = initialIntentionStrength;
		this.agent = agent;
		this.pickUpTimeInDistance = pickUpTimeInDistance;
	}
	
	public int getPickUpTimeInDistance(){
		return pickUpTimeInDistance;
	}
	
	public Truck getTruck() {
		return truck;
	}
	
	public void resetStrength(){
		strength = initialIntentionStrength;
	}
	
	public int getStrength() {
		return strength;
	}

	public PackageAgent getAgent() {
		return agent;
	}
	
	public void devaluate(){
		strength--;
	}

	public boolean isBetterThan(Intention intention){
		if(intention.getAgent() != agent){
			return false;
		}
		
		// if this < new *1.2 return this
		if (this.getPickUpTimeInDistance() <  ( intention.getPickUpTimeInDistance() * (1 + betterPercentage) ) )
			return true;
		else
			return false;
		
	}

}
