package environment;

import java.util.Collection;
import java.util.HashMap;

import rinde.sim.core.graph.Point;

public class EnvironmentMgr {
	
	private HashMap<Point, Intersection> intersectionPoints = new HashMap<Point, Intersection>();
	
	public EnvironmentMgr(){
		
	}
	
	public Intersection getIntersection(Point point){
		return intersectionPoints.get(point);
	}
	
	public void addIntersection(Intersection intersection){
		intersectionPoints.put(intersection.getPosition(), intersection);
	}
	
	public Collection<Intersection> getIntersections(){
		return intersectionPoints.values();
	}

}
