package environment;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;

import agents.PackageAgent;
import agents.Truck;

import rinde.sim.core.graph.Point;

public class Intersection {
	
	private final Point position;
	
	private HashMap<PackageAgent, IntersectionPackageProp> availablePackages = new HashMap<PackageAgent, IntersectionPackageProp>();

	/**
	 * CONSTRUCTOR
	 * @param position
	 */
	public Intersection(Point position){
		this.position = position;
	}
	
	/**
	 * Get position of the intersection
	 * 
	 * @return
	 */
	public Point getPosition(){
		return position;
	}
	
	/**
	 * Add a packageAgent to the intersection
	 * 
	 * @param packageAgent
	 */
	public void addPackageAgent(PackageAgent packageAgent, Point location ){
		if(availablePackages.containsKey(packageAgent)){
			IntersectionPackageProp properties = availablePackages.get(packageAgent);
			properties.updateAvailability();
		}
		else{
			IntersectionPackageProp properties = new IntersectionPackageProp(location, this, packageAgent);
			availablePackages.put(packageAgent, properties );
		}
	}
	
	/**
	 * Intend a package to pick up
	 * 
	 * @param packageAgent
	 */
	public void setPackageAsIntented(PackageAgent packageAgent, Intention intention){
		IntersectionPackageProp agentProperties = availablePackages.get(packageAgent);
		agentProperties.setAsIntended(intention);
	}
	
	/**
	 * Devaluates intentionStrengths and availabilityStrengths
	 */
	public void devaluate(){
		Set<PackageAgent> keys = availablePackages.keySet();
		try{
			for (PackageAgent packageAgent : keys){
				availablePackages.get(packageAgent).devaluate();
			}
		}
		catch(Exception e){}
	}
	
	/**
	 * Relives a packageAgent from this intersection
	 * 
	 * @param packageAgent
	 */
	public void removePackageAgent(PackageAgent packageAgent){
		availablePackages.remove(packageAgent);
	}
	
	public ArrayList<IntersectionPackageProp> getAvailablePackages(){
		Set<PackageAgent> keys = availablePackages.keySet();
		ArrayList<IntersectionPackageProp> intersectionProperties = new ArrayList<IntersectionPackageProp>();
		for(PackageAgent packageAgent : keys){
			intersectionProperties.add(availablePackages.get(packageAgent));
		}
		return intersectionProperties;
	}

}
