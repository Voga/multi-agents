package environment;

import java.util.ArrayList;

import agents.PackageAgent;
import agents.Truck;

import rinde.sim.core.graph.Point;

public class IntersectionPackageProp {
	
	private int availabilityStrength;
	private Intention intention;
	private Point location;
	private Intersection intersection;
	private PackageAgent packageAgent;
	
	private int initialAvailabilityStrength = 5;
	
	public IntersectionPackageProp(Point location, Intersection intersection, PackageAgent packageAgent){
		availabilityStrength = initialAvailabilityStrength;
		this.intersection = intersection;
		this.location = location;
		this.packageAgent = packageAgent;
	}
	
	public void setAsIntended(Intention intention){
		this.intention = intention;
		this.intention.resetStrength();
	}
	
	public void updateAvailability(){
		availabilityStrength = initialAvailabilityStrength;
	}
	
	
	public int getAvailabilityStrength(){
		return availabilityStrength;
	}
	
	public Intention getIntention(){
		return intention;
	}
	
	public Point getLocation() {
		return location;
	}

	public void setLocation(Point location) {
		this.location = location;
	}

	public void devaluate(){
		devaluateIntention();
		devaluateAvailabilityStrength();
	}
	
	public boolean polseIntention(Truck truck, Intention intention){
		if(this.intention == null || this.intention.getTruck() == truck){
			return true;
		}
		if(intention.isBetterThan(this.intention)){
			return true;
		}
		return false;
	}
	
	private void devaluateIntention(){
		if(intention != null){
			if (intention.getStrength() == 0){
				intention = null;
			}
			else { 
				intention.devaluate();
			}
		}
	}
	
	private void devaluateAvailabilityStrength(){
		if (availabilityStrength == 1){
			intersection.removePackageAgent(packageAgent);
		}
		else{
			availabilityStrength--;
		}
	}
	
	public PackageAgent getPackageAgent(){
		return packageAgent;
	}
	
	@Override
	public boolean equals(Object o){
		if (o instanceof IntersectionPackageProp)
			return (this.getPackageAgent().equals(((IntersectionPackageProp) o).getPackageAgent()));
		else return false;
	}
	

}
