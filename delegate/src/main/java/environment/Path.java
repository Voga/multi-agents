package environment;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

import agents.PackageAgent;
import agents.Truck;

import rinde.sim.core.graph.Point;

public class Path {
	private int length;
	private List<PackageAgent> packagesOnRoute;
	private LinkedList<Point> path;
	private Truck truck;
	private Iterator<PackageAgent> iterator;
	
	/**
	 * 
	 * @param truck
	 * @param packageProps: only the packageprops on the intersections, deliveries not included!!
	 */
//	public Path(Truck truck, ArrayList<IntersectionPackageProp> packageProps){
//		this.truck = truck;
//		packagesOnRoute = new ArrayList<PackageAgent>();
//		for(IntersectionPackageProp prop: packageProps){
//			packagesOnRoute.add(prop.getPackageAgent());
//		}
//		this.path = calculatePath();
//		this.length = calculateLength(path);
//	}
	
	/**
	 * 
	 * @param truck
	 * @param packagesOnRoute: only the packageAgents, deliveries not included!!
	 */
	public Path(Truck truck, List<PackageAgent> packagesOnRoute){
		this.truck = truck;
		this.packagesOnRoute = packagesOnRoute;
		this.path = calculatePath();
		this.length = calculateLength(path);
		iterator = packagesOnRoute.iterator();
	}
	
//	public PackageAgent popPackage(){
//		try{
//			PackageAgent agent = packagesOnRoute.get(0);
//			packagesOnRoute.remove(0);
//			return agent;
//		}
//		catch(IndexOutOfBoundsException e){
//			return null;
//		}
//	}
	
	public PackageAgent popPackage(){
		if(iterator.hasNext()){
			PackageAgent p = iterator.next();
			return p;
		}
		return null;
	}
	
	public void removePackage(){
		iterator.remove();
	}

	private LinkedList<Point> calculatePath() {
		LinkedList<Point> points = new LinkedList<Point>();
		Point currentPosition = truck.getPosition();
		for (PackageAgent packageAgent : packagesOnRoute) {
			List<Point> test = truck.getRoadModel().getShortestPathTo(
					currentPosition, packageAgent.getPackage().getPickupLocation());
			points.addAll(truck.getRoadModel().getShortestPathTo(
					currentPosition, packageAgent.getPackage().getPickupLocation()));
			points.remove(points.size()-1);
			currentPosition = packageAgent.getPackage()
					.getDeliveryLocation();
			points.addAll(truck.getRoadModel().getShortestPathTo(
					packageAgent.getPackage().getPickupLocation(),currentPosition));
			points.remove(points.size()-1);
		}
		points.add(currentPosition);
		return points;
	}

	private int calculateLength(List<Point> points) {
		int length = 0;
		Point point1 = points.get(0);
		points.remove(0);
		for (Point point2 : points) {
			length += truck.getRoadModel().getGraph().connectionLength(point1, point2);
			point1 = point2;
		}
		return length;
	}
	
	public int getLength() {
		return length;
	}

	public List<PackageAgent> getPackagesOnRoute() {
		return packagesOnRoute;
	}

	public LinkedList<Point> getPath() {
		return path;
	}

	public Truck getTruck() {
		return truck;
	}
	
	/**
	 * gets the length from the path without the deliverydistance from the last package
	 * @return
	 */
	public int getLengthToEndPackage(Point startPoint){
		LinkedList<Point> points = new LinkedList<Point>();
		Point currentPosition = startPoint;
		int i = 0;
		while ( (i <= packagesOnRoute.size()-2) && (packagesOnRoute.size() > 1) ) {
			points.addAll(truck.getRoadModel().getShortestPathTo(
					currentPosition, packagesOnRoute.get(i).getPackage().getPickupLocation()));
			points.remove(points.size()-1);
			currentPosition = packagesOnRoute.get(i).getPackage()
					.getDeliveryLocation();
			points.addAll(truck.getRoadModel().getShortestPathTo(
					packagesOnRoute.get(i).getPackage().getPickupLocation(),currentPosition));
			points.remove(points.size()-1);
			i++;
		}
		points.addAll(truck.getRoadModel().getShortestPathTo(
				currentPosition, packagesOnRoute.get(packagesOnRoute.size()-1).getPackage().getPickupLocation()));
		return calculateLength(points);
		
	}
	
	
	
	public int getLengthToEndPackage(){
		return getLengthToEndPackage(truck.getPosition());
	}
}
