package agents;

import java.util.HashMap;

import main.DeliveryLocation;
import main.StatisticsMgr;

import rinde.sim.core.SimulatorAPI;
import rinde.sim.core.SimulatorUser;
import rinde.sim.core.graph.Point;
import rinde.sim.core.model.RoadModel;
import rinde.sim.core.model.RoadUser;

public class Package implements SimulatorUser, RoadUser{
	public final String packageID;
	private Point pickupLocation;
	private DeliveryLocation deliveryLocation;
	private boolean pickedUp;
	private boolean delivered;
	private RoadModel model;
	private SimulatorAPI simulator;
	private PackageAgent agent;
	private int ticksAge = 0;
	public Package(String packageID, Point pickupLocation, DeliveryLocation deliveryLocation) {
		this.packageID = packageID;
		this.pickupLocation = pickupLocation;
		this.deliveryLocation = deliveryLocation;
		this.pickedUp = false;
		this.delivered = false;
	}
	
	public int getTicksAge(){
		return ticksAge;
	}
	
	public void increaseTicksAge(){
		ticksAge++;
	}
	
	public boolean needsPickUp(){
		return !pickedUp;
	}
	
	public void setAgent(PackageAgent agent){
		this.agent = agent;
	}
	
	public PackageAgent getAgent(){
		return agent;
	}

	public boolean delivered(){
		this.simulator.unregister(agent);
		return delivered;
	}
	
	public void pickup(){
		this.pickedUp = true;
		this.simulator.unregister(this);
		StatisticsMgr.getInstance().increaseTotalTicksAgePickUp(ticksAge);
	}
	
	public void deliver(){
		this.delivered = true;
		this.simulator.unregister(deliveryLocation);
		StatisticsMgr.getInstance().increaseTotalTicksAgeDelivery(ticksAge);
	}
	
	public String getPackageID(){
		return packageID;
	}
	
	@Override
	public String toString() {
		return packageID;
	}

	public Point getPickupLocation(){
		return pickupLocation;
	}
	
	public Point getDeliveryLocation(){
		return deliveryLocation.getPosition();
	}

	@Override
	public void setSimulator(SimulatorAPI api) {
		this.simulator = api;
	}

	@Override
	public void initRoadUser(RoadModel model) {
		model.addObjectAt(this, pickupLocation);
		this.model = model;
	}
	
	public RoadModel getRoadModel(){
		return model;
	}
	
}
