package agents;

import java.util.ArrayList;
import java.util.Collection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import environment.EnvironmentMgr;
import environment.Intention;
import environment.Intersection;

import rinde.sim.core.Simulator;
import rinde.sim.core.SimulatorAPI;
import rinde.sim.core.SimulatorUser;
import rinde.sim.core.TickListener;
import rinde.sim.core.graph.Point;
import rinde.sim.core.model.RoadModel;
import rinde.sim.core.model.communication.CommunicationAPI;
import rinde.sim.core.model.communication.CommunicationUser;
import rinde.sim.core.model.communication.Mailbox;
import rinde.sim.core.model.communication.Message;

public class PackageAgent implements TickListener, SimulatorUser, CommunicationUser {
	protected static final Logger LOGGER = LoggerFactory.getLogger(Simulator.class); 

	private SimulatorAPI simulator;
	private Package myPackage;
	private CommunicationAPI communicationAPI;
	private Mailbox mailbox;
	private double radius;
	private double reliability;
	private EnvironmentMgr environment;
	private final static int updateInterval = 5; //TODO: at least the same of truckagent..? trucks need to resend their intentions.
	private int time = 0;
	private static final int maxDepth = 10;
	private Intention intention;
	private boolean needIntentionBroadcast = false;
	
	
	public PackageAgent(Package myPackage, double radius, double reliability, EnvironmentMgr environment){
		this.myPackage = myPackage;
		myPackage.setAgent(this);
		this.radius = radius;
		this.reliability = reliability;
		this.mailbox = new Mailbox();
		this.environment = environment;
	}
	
	@Override
	public void setSimulator(SimulatorAPI api) {
		this.simulator = api;
	}
	
	@Override
	public void tick(long currentTime, long timeStep) {
		myPackage.increaseTicksAge();
		if (isValidTime() && myPackage.needsPickUp()){
			devaluateIntentionStrength();
			Point location = this.myPackage.getPickupLocation();
			ArrayList<Point> donePoints = new ArrayList<Point>();
			donePoints.add(location);
			updateLocation(location, donePoints, 0); 
			needIntentionBroadcast = false;
		}
	}

	private void updateLocation(Point location, ArrayList<Point> donePoints, int depth) {
		Intersection intersection = environment.getIntersection(location);
		intersection.addPackageAgent(this, myPackage.getPickupLocation());
		broadcastIntention(intersection);
		donePoints.add(location);
		Collection<Point> outgoingNodes = myPackage.getRoadModel().getGraph().getOutgoingConnections(location);
		for(Point point : outgoingNodes){
			if(!donePoints.contains(point)){
				if(depth <= maxDepth)
					updateLocation(point, donePoints, depth++);
			}
		}
	}
	
	private void broadcastIntention(Intersection intersection) {
		if(needIntentionBroadcast){
			intersection.setPackageAsIntented(this, new Intention(this.intention.getTruck(), this, this.intention.getPickUpTimeInDistance()));
		//	needIntentionBroadcast = false;
		}
	}

	public boolean isValidTime(){
		if(time % updateInterval == 0){
			time = 1;
			return true;
		}
		else{
			time++;
			return false;
		}
	}
	
	public void devaluateIntentionStrength(){
		if(intention != null){
			if (intention.getStrength() == 0){
				intention = null;
			}
			else { 
				intention.devaluate();
			}
		}
	}
	
	public boolean expressIntention(Truck truck, int pickUpTimeInDistance){
		Intention newIntention = new Intention(truck, this, pickUpTimeInDistance);
		if(intention == null){
			intention = newIntention;
			needIntentionBroadcast = true;
			return true;
		}
		if(intention.getTruck() == truck){ //huidige truck kan pad altijd aanpassen
			this.intention = newIntention;
			needIntentionBroadcast = true;
			return true;
		}
		if( !(intention.isBetterThan(newIntention)) ){
			intention = newIntention;
			needIntentionBroadcast = true;
			return true;
		}
		return false;
	}

	@Override
	public void afterTick(long currentTime, long timeStep) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setCommunicationAPI(CommunicationAPI api) {
		this.communicationAPI = api;
	}

	@Override
	public Point getPosition() {
		//TODO
		return this.myPackage.getPickupLocation();
	}

	@Override
	public double getRadius() {
		return radius;
	}

	@Override
	public double getReliability() {
		return reliability;
	}

	@Override
	public void receive(Message message) {
		this.mailbox.receive(message);
	}
	
	public Package getPackage(){
		return myPackage;
	}

}
