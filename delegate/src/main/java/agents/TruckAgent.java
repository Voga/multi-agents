package agents;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Random;
import java.util.Set;

import main.Delegate;
import main.StatisticsMgr;

import org.junit.experimental.max.MaxCore;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import environment.EnvironmentMgr;
import environment.Intention;
import environment.Intersection;
import environment.IntersectionPackageProp;
import environment.Path;

import rinde.sim.core.Simulator;
import rinde.sim.core.SimulatorAPI;
import rinde.sim.core.SimulatorUser;
import rinde.sim.core.TickListener;
import rinde.sim.core.graph.Point;
import rinde.sim.core.model.RoadModel;
import rinde.sim.core.model.RoadModel.PathProgress;
import rinde.sim.core.model.communication.CommunicationAPI;
import rinde.sim.core.model.communication.CommunicationUser;
import rinde.sim.core.model.communication.Mailbox;
import rinde.sim.core.model.communication.Message;

public class TruckAgent implements TickListener, SimulatorUser,
CommunicationUser {

	protected static final Logger LOGGER = LoggerFactory
			.getLogger(Simulator.class);
	private static int globalId = 1;
	private SimulatorAPI simulator;
	private Queue<Point> pathPoints;
	private Truck truck;
	private CommunicationAPI communicationAPI;
	private double reliability, radius;
	private Mailbox mailbox;
	private Random randomGenerator = new Random();
	private PackageAgent agent;
	private int id;
	private EnvironmentMgr environment;
	private int time;
	private int maxSearchDepth = 5;
	private static int maxPackagesConsecutive; 
	private final static int updateInterval = 5;
	private Path path;

	public TruckAgent(Truck truck, double radius, double reliability,
			EnvironmentMgr environment) {
		maxPackagesConsecutive = Delegate.getMaxPackagesConsecutive();
		this.id = globalId;
		globalId++;
		this.truck = truck;
		this.radius = radius;
		this.reliability = reliability;
		this.mailbox = new Mailbox();
		this.environment = environment;
		truck.setTruckAgent(this);
	}

	@Override
	public void setSimulator(SimulatorAPI api) {
		this.simulator = api;
	}
	
	public void removeTruck(){
		simulator.unregister(truck);
		simulator.unregister(this);
	}

	/**
	 * Very dumb agent, that chooses paths randomly and tries to pickup stuff
	 * and deliver stuff at the end of his paths
	 */
	@Override
	public void tick(long currentTime, long timeStep) {
		if (path == null && isValidTime() ) {
			pathPoints = null;
			Point truckLocation = this.getPosition();
			ArrayList<ArrayList<PackageAgent>> foundPaths = findPathsRecursive(truckLocation, maxPackagesConsecutive - 1);
			if(foundPaths != null && !foundPaths.isEmpty()){
				ArrayList<Path> paths = createPaths(foundPaths);
				Path minimumPath = calculateMinimumPath(paths);
				List<PackageAgent> packageAgents = minimumPath.getPackagesOnRoute();
				if(sendIntentionToPackages(packageAgents)){ //only set path if intentions succeed!		
					pathPoints = null;
					path = minimumPath;
				}
			}
		}
		
		if(path != null && (pathPoints == null || pathPoints.isEmpty()) && !truck.hasLoad() && agent != null && truck.getPosition() == agent.getPackage().getPickupLocation()){
			if(truck.tryPickup()){
				path.removePackage();
			}
		}
		
		if(path != null && (pathPoints == null || pathPoints.isEmpty()) && !truck.hasLoad()){
			agent = path.popPackage();
			if(agent != null)
				pathPoints = new LinkedList<Point>(truck.getRoadModel().getShortestPathTo(truck.getPosition(), agent.getPackage().getPickupLocation()));
			else
				path = null;
		}
		
		if(truck.hasLoad() && (pathPoints == null || pathPoints.isEmpty())){
			if(!truck.tryDelivery()){
				pathPoints = new LinkedList<Point>(truck.getRoadModel().getShortestPathTo(truck.getPosition(), agent.getPackage().getDeliveryLocation()));
			}
			else {
				agent = null;
			}
		}
		if(path != null && isValidTime()){
			List<PackageAgent> packageAgents = path.getPackagesOnRoute();
			if(!sendIntentionToPackages(packageAgents)){
				path = null;	
			}
		}
		if(!(pathPoints == null || pathPoints.isEmpty())){
			PathProgress progress = truck.drive(pathPoints, timeStep);
			StatisticsMgr.getInstance().increaseDistanceTraveled(progress.distance);
		}
	}

	private boolean sendIntentionToPackages(List<PackageAgent> packageAgents) {
		int i = 0;
		while(i <= (packageAgents.size() -1)){
			Path tempPath = new Path(truck, packageAgents.subList(0, i+1));
			int pickupTimeInDistanceForI = 0;
			if(truck.hasLoad()){
				List<Point> deliveryList = truck.getRoadModel().getShortestPathTo(this.getPosition(), truck.getLoad().getDeliveryLocation());
				pickupTimeInDistanceForI = calculateLength(deliveryList);
				pickupTimeInDistanceForI += tempPath.getLengthToEndPackage(truck.getLoad().getDeliveryLocation());
			}
			else{
				pickupTimeInDistanceForI += tempPath.getLengthToEndPackage();
			}
			if(!packageAgents.get(i).expressIntention(truck, pickupTimeInDistanceForI)){
				return false;
			}
			i++;
		}
		return true;
	}

	private ArrayList<ArrayList<PackageAgent>> findPathsRecursive(
			Point truckLocation, int i) {
		while(i >= 0){
			ArrayList<ArrayList<PackageAgent>> result = findPaths(truckLocation, null, i);
			if(result != null && !result.isEmpty())
				return result;
			i--;
		}
		return null;
	}

	private Path calculateMinimumPath(ArrayList<Path> paths) {
		int min = Integer.MAX_VALUE;
		Path minimumPath = null;
		for(Path current: paths){
			if(current.getLength() <= min){
				min = current.getLength();
				minimumPath = current;
			}
		}
		return minimumPath;
	}

	private ArrayList<Path> createPaths(ArrayList<ArrayList<PackageAgent>> foundPaths) {
		ArrayList<Path> result = new ArrayList<Path>();
		for (ArrayList<PackageAgent> path : foundPaths) {			
			result.add(new Path(truck, path));
		}
		return result;
	}

	/**
	 * Finds the packages on and around a location with given search depth
	 * 
	 * @param location
	 * @param donePoints
	 * @param depth
	 * @return
	 */
	private ArrayList<IntersectionPackageProp> findAroundPackages(
			Point location, ArrayList<Point> donePoints, ArrayList<IntersectionPackageProp> tempPackages, int depth) {
		
		if(depth == 0){
			return new ArrayList<IntersectionPackageProp>();
		}

		//ArrayList<IntersectionPackageProp> foundPackages = new ArrayList<IntersectionPackageProp>();
		if (environment.getIntersection(location) != null) {
			tempPackages = addWithoutDoubles(tempPackages, environment.getIntersection(location).getAvailablePackages());
		}
		else {
			if(location instanceof RoadModel.MidPoint)
			location = ((RoadModel.MidPoint) location).loc.to;
		}

		Collection<Point> outgoingNodes = truck.getRoadModel().getGraph()
				.getOutgoingConnections(location);

		for (Point point : outgoingNodes) {
			if (!donePoints.contains(point)) {

				donePoints.add(point);

				tempPackages = addWithoutDoubles(tempPackages, findAroundPackages(point, donePoints, tempPackages, depth--));
			}
		}

		return tempPackages;
	}

	
	
	private ArrayList<IntersectionPackageProp> addWithoutDoubles(ArrayList<IntersectionPackageProp> list1,ArrayList<IntersectionPackageProp> list2) {
		for (IntersectionPackageProp packageProp : list2){
			if(!list1.contains(packageProp)){
				list1.add(packageProp);
			}
		}
		return list1;
	}

	/**
	 * Finds the paths from a location with breadth first search: find packages,
	 * mentioned on intersection, around the location (findAroundPackages) take
	 * from that list the first package p. find around packages of
	 * deliverylocation of p. append aroundpackages to p. add to the back repeat
	 * 
	 * @param initialLocation
	 * @param tempPaths
	 *            = null
	 * @param initial
	 *            = null
	 * @param depth
	 * @return
	 */

	private ArrayList<ArrayList<PackageAgent>> findPaths(Point initialLocation, ArrayList<ArrayList<PackageAgent>> tempPaths, int depth) {
	
		// FIRST TIME: tempPaths = null
		if (tempPaths == null) {
			tempPaths = new ArrayList<ArrayList<PackageAgent>>();
			ArrayList<IntersectionPackageProp> initialAroundPackages = findAroundPackages(initialLocation, new ArrayList<Point>(), new ArrayList<IntersectionPackageProp>(), maxSearchDepth);
			try {initialAroundPackages.get(0);} catch(Exception e){return null;}
			for(IntersectionPackageProp prop : initialAroundPackages){
				ArrayList<PackageAgent> tempList = new ArrayList<PackageAgent>();
				tempList.add(prop.getPackageAgent());
				Path tempPath = new Path(truck, tempList);
				//package added..? check intentions
				Intention newIntention = new Intention(this.truck, prop.getPackageAgent(), tempPath.getLengthToEndPackage());
				if(prop.getIntention() == null || !prop.getIntention().isBetterThan(newIntention) || truck == prop.getIntention().getTruck()){ //huidige truck kan altijd wijzigen
					tempPaths.add(tempList);
				}
			}
			tempPaths.add(new ArrayList<PackageAgent>()); //marker !!
		}
		
		// Handle depth is 0 in the argument
		if(depth == 0){
			tempPaths.remove(tempPaths.size()-1);
			return tempPaths;
		}

		// breadth first search
		ArrayList<PackageAgent> workPath = tempPaths.get(0);

		if(workPath.isEmpty()) {//MARKER FOUND -> FULL CYCLE DONE
			tempPaths.remove(0);
			if(depth == 1){//stop
				return tempPaths;
			}
			tempPaths.add(workPath);//put marker in the back
			return findPaths(initialLocation, tempPaths, depth-1); //repeat with new depth
		}
		else{//no full cycle
			tempPaths.remove(0);
			PackageAgent lastNode = workPath.get(workPath.size() -1 );

			ArrayList<IntersectionPackageProp> lastNodeLocalPackages = findAroundPackages(lastNode.getPackage().getDeliveryLocation(), new ArrayList<Point>(), new ArrayList<IntersectionPackageProp>(), maxSearchDepth);
			for(IntersectionPackageProp node : lastNodeLocalPackages){
				ArrayList<PackageAgent> tempList = new ArrayList<PackageAgent>();
				tempList.addAll(workPath);
				tempList.add(node.getPackageAgent());
				Path tempPath = new Path(truck, tempList);
				//package added..? check intentions
				Intention newIntention = new Intention(this.truck, node.getPackageAgent(), tempPath.getLengthToEndPackage());
				if(node.getIntention() == null || !node.getIntention().isBetterThan(newIntention) || truck == node.getIntention().getTruck()){ //huidige truck kan altijd wijzigen
					tempPaths.add(tempList);
				}
				else{
					if(!tempPaths.contains(workPath)){
						tempPaths.add(workPath);
					}
				}
			}
			tempPaths = removeWrongPaths(tempPaths);
		}

		//repeat
		return findPaths(initialLocation, tempPaths, depth);
	}

	private ArrayList<ArrayList<PackageAgent>> removeWrongPaths(	ArrayList<ArrayList<PackageAgent>> tempPaths) {
		ArrayList<ArrayList<PackageAgent>> returnList = new ArrayList<ArrayList<PackageAgent>>();
		
		for (ArrayList<PackageAgent> list : tempPaths){
			
			Set<PackageAgent> tempPackageSet = new HashSet<PackageAgent>(list);
			if(tempPackageSet.size() < list.size()){
				//DOUBLES
			}
			else{
				returnList.add(list);
			}
		}
		return returnList;
	}

	public boolean isValidTime() {
		if (time % updateInterval == 0) {
			time = 1;
			return true;
		} else {
			time++;
			return false;
		}
	}

	@Override
	public void afterTick(long currentTime, long timeStep) {
		// TODO Auto-generated method stub

	}

	@Override
	public void setCommunicationAPI(CommunicationAPI api) {
		this.communicationAPI = api;
	}

	@Override
	public Point getPosition() {
		return this.truck.getPosition();
	}

	@Override
	public double getRadius() {
		return this.radius;
	}

	@Override
	public double getReliability() {
		return this.reliability;
	}

	@Override
	public void receive(Message message) {
		this.mailbox.receive(message);
	}

	public int getId() {
		return id;
	}

	public Truck getTruck() {
		return truck;
	}
	
	private int calculateLength(List<Point> points) {
		int length = 0;
		Point point1 = points.get(0);
		points.remove(0);
		for (Point point2 : points) {
			length += truck.getRoadModel().getGraph().connectionLength(point1, point2);
			point1 = point2;
		}
		return length;
	}

}
