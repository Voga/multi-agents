package agents;

import java.util.Collection;

import environment.EnvironmentMgr;
import environment.Intersection;
import rinde.sim.core.TickListener;

public class EnvironmentAgent implements TickListener{
	
	private EnvironmentMgr environment;
	private int time;
	private final static int updateInterval = 5;
	
	public EnvironmentAgent(EnvironmentMgr environment){
		this.environment =  environment;
	}
	
	@Override
	public void tick(long currentTime, long timeStep) {
		if(isValidTime()){
			Collection<Intersection> intersections = environment.getIntersections();
			for(Intersection intersection: intersections){
				intersection.devaluate();
			}
		}
	}

	@Override
	public void afterTick(long currentTime, long timeStep) {
		// TODO Auto-generated method stub
		
	}
	
	public boolean isValidTime(){
		if(time % updateInterval == 0){
			time = 1;
			return true;
		}
		else{
			time++;
			return false;
		}
	}

}
