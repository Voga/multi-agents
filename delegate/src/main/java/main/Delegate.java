package main;

import java.util.Scanner;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.PropertyConfigurator;

import environment.Intention;

import rinde.sim.event.pdp.StandardType;
import rinde.sim.scenario.Scenario;
import rinde.sim.scenario.ScenarioBuilder;
import rinde.sim.scenario.ScenarioBuilder.TimeSeries;
import rinde.sim.scenario.TimedEvent;
import rinde.sim.util.OSM;

public class Delegate {

	private static double betterPercentage = 0.2; //standard intention better percentage
	private static int maxPackagesConsecutive = 2; //standard truckagent packages consecutive

	public static void main(String[] args) throws Exception{
		
		//weightfactortest
//		int a = 7;
//		int b = 6;
//		double c = 1.2;
//		if( a < b*c){System.out.println("yes");}
		
		PropertyConfigurator.configure("log4j.conf");

		boolean automatic;

		Scanner scanner = new Scanner(System.in);
		while(true){
			System.out.println("Automatic testruns?(Y/N)");
			String removeString = scanner.next();
			if(removeString.toLowerCase().equals("y")){
				automatic = true;
				break;
			}
			else if(removeString.toLowerCase().equals("n")){
				automatic= false;
				break;
			}
			System.out.println("Please answer with Y or N");
		}

		//user run
		if(!automatic){
			scanner = new Scanner(System.in);
			System.out.println("Amount of trucks in the simulation:");
			int amountTrucks = scanner.nextInt();
			System.out.println("Amount of packages in the simulation:");
			int amountPackages = scanner.nextInt();
			boolean remove = false;
			while(true){
				System.out.println("Remove truck periodically?(Y/N)");
				String removeString = scanner.next();
				if(removeString.toLowerCase().equals("y")){
					remove = true;
					break;
				}
				else if(removeString.toLowerCase().equals("n")){
					remove= false;
					break;
				}
				System.out.println("Please answer with Y or N");
			}
			run(amountTrucks, amountPackages, remove);
		}
		
		
		//predefined run
		else{
			double percentageBetter = 0;
			while (percentageBetter <= 1){
				int nbTrucks = 5;
				int nbPackages = 20;
				betterPercentage = percentageBetter;
				System.out.println(nbTrucks+" trucks - "+nbPackages+" packages - weight: "+ betterPercentage);
				runOnlyMAS(nbTrucks,nbPackages,false);
				percentageBetter += 0.2;
			}			
			
			int packagesConsecutive = 2;	
			while (packagesConsecutive <= 5){
				int nbTrucks = 3;
				int nbPackages = 3;
				maxPackagesConsecutive = packagesConsecutive;
				System.out.println(nbTrucks + " trucks - " + nbPackages +" packages - packages: "+ maxPackagesConsecutive);
				runOnlyMAS(nbTrucks,nbPackages,false);
				packagesConsecutive += 1;
			}
			
		}
	}

	public static void run(int nbTrucks, int nbPackages, boolean remove) throws Exception{
		Scenario scenario = setUpBuilder(nbTrucks, nbPackages, true, remove);
		final String MAP_DIR = "files/maps/";
		//new SimpleController(scenario, -1, OSM.parse(MAP_DIR+"rumst.osm"));
		new SimpleController(scenario, -1, MAP_DIR + "leuven-simple.dot");
		scenario = setUpBuilder(nbTrucks, nbPackages, false, remove);
		new rinde.sim.lab.session2.gradient_field_exercise.SimpleController(scenario, -1, MAP_DIR + "leuven-simple.dot");
		new rinde.sim.lab.session3.cnet.SimpleController(scenario, -1, MAP_DIR + "leuven-simple.dot");
	}
	
	public static void runOnlyMAS(int nbTrucks, int nbPackages, boolean remove) throws Exception{
		Scenario scenario = setUpBuilder(nbTrucks, nbPackages, true, remove);
		final String MAP_DIR = "files/maps/";
		//new SimpleController(scenario, -1, OSM.parse(MAP_DIR+"rumst.osm"));
		new SimpleController(scenario, -1, MAP_DIR + "leuven-simple.dot");
	}

	private static Scenario setUpBuilder(int amountTrucks, int amountPackages, boolean delegate, boolean remove){
		ScenarioBuilder builder = new ScenarioBuilder(StandardType.ADD_TRUCK, StandardType.ADD_PACKAGE, StandardType.REMOVE_TRUCK, CostumEvent.INIT_ENV);
		builder.add(
				new ScenarioBuilder.MultipleEventGenerator<TimedEvent>(
						0, //at time 0
						amountTrucks,
						new ScenarioBuilder.EventTypeFunction(
								StandardType.ADD_TRUCK
								)
						)
				);

		builder.add(
				new ScenarioBuilder.MultipleEventGenerator<TimedEvent>(
						5, //at time 0
						amountPackages,
						new ScenarioBuilder.EventTypeFunction(
								StandardType.ADD_PACKAGE
								)
						)
				);
		if(delegate){
			builder.add(
					new ScenarioBuilder.MultipleEventGenerator<TimedEvent>(
							0, //at time 0
							1,
							new ScenarioBuilder.EventTypeFunction(
									CostumEvent.INIT_ENV
									)
							)
					);
		}
		if(remove){
			builder.add(
					new ScenarioBuilder.MultipleEventGenerator<TimedEvent>(
							5000000, //at time 0
							1,
							new ScenarioBuilder.EventTypeFunction(
									StandardType.REMOVE_TRUCK
									)
							)
					);
		}
		return builder.build();
	}

	public enum CostumEvent{
		INIT_ENV;
	}

	public static double getSignificantIntentionWeight() {
		return betterPercentage;
	}

	public static int getMaxPackagesConsecutive() {
		return maxPackagesConsecutive ;
	}
}
