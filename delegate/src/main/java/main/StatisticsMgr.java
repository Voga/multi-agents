package main;

public class StatisticsMgr {
	
	private static StatisticsMgr statistics;
	private long totalTicksAgePickUp = 0;
	private long totalTicksAgeDelivery = 0;
	private double distanceTraveled = 0;
	private String solutionName;
	
	public synchronized static StatisticsMgr getInstance(){
		if(statistics == null)
			statistics = new StatisticsMgr();
		return statistics;
	}
	
	public static void reset(){
		statistics = new StatisticsMgr();
	}
	
	public void setSolutionName(String name){
		solutionName = name;
	}
	
	public synchronized void increaseTotalTicksAgePickUp(int amount){
		totalTicksAgePickUp += amount;
	}
	
	public synchronized void increaseTotalTicksAgeDelivery(int amount){
		totalTicksAgeDelivery += amount;
	}
	
	public synchronized void increaseDistanceTraveled(double amount){
		distanceTraveled += amount;
	}
	
	public void printStatistics(){
		System.out.println("System Statistics for "+solutionName);
		System.out.println("-----------------");
		System.out.println("Total time packages had to wait for pickup: "+totalTicksAgePickUp);
		System.out.println("Total time packages had to wait for delivery: "+totalTicksAgeDelivery);
		System.out.println("Total driving distance: "+distanceTraveled);
		System.out.println("-----------------");
	}
}
